package Steps;

import java.io.IOException;

import Pages.CartPageOpenCart;
import Pages.HomePageOpenCart;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TestSeleniumSteps {
	
	HomePageOpenCart homeopencart = new HomePageOpenCart();
	CartPageOpenCart cartopencart = new CartPageOpenCart();
	
	@Given("Abrir la web Opencart")
	public void abrir_la_web_opencart() {
		homeopencart.openUrlOpencart();
	}
	@When("ingresar el producto")
	public void ingresar_el_producto() {
		homeopencart.searchProduct("iPhone");
	}
	@And("seleccionar la primera opcion")
	public void seleccionar_la_primera_opcion() {
		homeopencart.selectProduct();
	}
	@And("agregar el producto al carrito")
	public void agregar_el_producto_al_carrito() {
		homeopencart.addToCart();
	}
	@And("presionar el boton del carrito")
	public void precionar_el_boton_del_carrito() throws InterruptedException {
		homeopencart.buttonCart();
		Thread.sleep(2000);
	}
	@And("ver el carrito")
	public void ver_el_carrito(){
		homeopencart.ViewCart();
	}
	@And("validar producto en el carrito")
	public void validar_producto_en_el_carrito() throws IOException {
		cartopencart.validateProductCart();	
		cartopencart.takeScreenshot();
	}
	@And("remover producto")
	public void remover_producto() throws InterruptedException {
		cartopencart.removeProductCart();
		Thread.sleep(2000);
	}
	@Then("validar producto removido")
	public void validar_producto_removido() throws IOException {
		cartopencart.cartEmpty();
		cartopencart.takeScreenshot();
		cartopencart.closeBrowser();
	}
	

			
		
	
	

}
