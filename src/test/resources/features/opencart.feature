Feature: Web Opencart

  Scenario: Buscar producto en web Opencart
    Given Abrir la web Opencart
    When ingresar el producto
    And seleccionar la primera opcion
    And agregar el producto al carrito
    And presionar el boton del carrito
    And ver el carrito
    And validar producto en el carrito
    And remover producto
    Then validar producto removido
